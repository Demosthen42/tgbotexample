import asyncio
from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import BotCommand

from food import register_handlers_food
from common import register_handlers_common

from vosk import Model, KaldiRecognizer
import wave
import os

async def set_commands(bot: Bot):
    commands = [
        BotCommand(command="/food", description="Заказать еду"),
        BotCommand(command="/start", description="Начать общение"),
        BotCommand(command="/cancel", description="Отменить действие"),
    ]
    await bot.set_my_commands(commands)


async def main():
    bot = Bot(token="5342760487:AAFvMxHOqwrtwigyEgRz7UMQGyOr-NN0prE")
    dp = Dispatcher(bot, storage=MemoryStorage())

    register_handlers_common(dp)
    register_handlers_food(dp)
    await set_commands(bot)
    await dp.skip_updates()
    await dp.start_polling()

def use_vosk():
    recognized_data = ""
    try:
        wave_audio = wave.open("./audio/789.wav", "rb")
        model = Model("./vosk-model")
        offline_recognizer = KaldiRecognizer(model, wave_audio.getframerate())

        data = wave_audio.readframes(wave_audio.getnframes())


        recognized_data = offline_recognizer.Result()
        print(recognized_data)
    except Exception as e:
        print(str(e))

if __name__ == "__main__":
    use_vosk()

    """запуск телеграм бота"""
    # asyncio.run(main())


    """тест бд"""
    # import sqlite3
    #
    # con = sqlite3.connect('db1')
    # cur = con.cursor()
    # for row in cur.execute('SELECT * FROM P'):
    #     print(row)
