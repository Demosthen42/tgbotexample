from aiogram import Bot, Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

food_names = ["1 блюдо", "2 блюдо", "3 блюдо"]
food_sizes = ["маленькую", "среднюю", "большую"]


class OrderFood(StatesGroup):
    waiting_for_food_name = State()
    waiting_for_food_size = State()


async def food_start(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for name in food_names:
        keyboard.add(name)
    await message.answer("Выберите блюдо:", reply_markup=keyboard)
    await OrderFood.waiting_for_food_name.set()


async def food_chosen(message: types.Message, state: FSMContext):
    if message.text not in food_names:
        await message.answer("Пожалуйста, выберите, используя кнопки")
        return
    await state.update_data(chosen_food=message.text)

    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for name in food_sizes:
        keyboard.add(name)
    await message.answer("Выберите размер порции:", reply_markup=keyboard)
    await OrderFood.next()


async def food_size_chosen(message: types.Message, state: FSMContext):
    if message.text not in food_sizes:
        await message.answer("Пожалуйста, выберите, используя кнопки")
        return

    await state.update_data(chosen_size_food=message.text)
    user_data = await state.get_data()

    await message.answer(f"Вы выбрали {user_data['chosen_size_food']} порцию {user_data['chosen_food']}",
                         reply_markup=types.ReplyKeyboardRemove())
    await state.finish()

def register_handlers_food(dp: Dispatcher):
    dp.register_message_handler(food_start, commands="food", state="*")
    dp.register_message_handler(food_chosen, state=OrderFood.waiting_for_food_name)
    dp.register_message_handler(food_size_chosen, state=OrderFood.waiting_for_food_size)
