from aiogram import Bot, Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

async def start(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Нажмите /foods чтобы заказать блюдо", reply_markup=types.ReplyKeyboardRemove())

async def cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Действие отменено", reply_markup=types.ReplyKeyboardRemove())

def register_handlers_common(dp: Dispatcher):
    dp.register_message_handler(start, commands="start", state="*")
    dp.register_message_handler(cancel, commands="cancel", state="*")
